const AUTHSTRINGS = Object.freeze({
    signIn: "Login",
    signUp: "Registration"
})

export { 
    AUTHSTRINGS
}
