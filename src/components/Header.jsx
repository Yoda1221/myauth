import { useSelector, useDispatch }     from 'react-redux'
import { FaSignInAlt, FaSignOutAlt }    from 'react-icons/fa'
import { useNavigate }                  from 'react-router-dom'
import { LinkContainer }                from 'react-router-bootstrap'
import { Navbar, Nav, Container, NavDropdown }  from 'react-bootstrap'
import { AUTHSTRINGS }                  from '../services/enums'
import { logout }                       from '../slices/authSlice'
import { useLogoutMutation }            from '../slices/usersApiSlice'

const Header = () => {
    const dispatch          = useDispatch()
    const navigate          = useNavigate()
    const [logoutApiCall]   = useLogoutMutation()
    const { userInfo }      = useSelector((state) => state.auth)

    const logoutHandler = async () => {
        try {
          await logoutApiCall().unwrap();
          dispatch(logout());
          navigate('/login');
        } catch (err) {
          console.error(err);
        }
    }

    return (
        <header>
            <Navbar bg='dark' variant='dark' expand='md' collapseOnSelect>
                <Container>
                    <LinkContainer to='/'>
                        <Navbar.Brand>My Auth</Navbar.Brand>
                    </LinkContainer>
                    <Navbar.Toggle aria-controls='basic-navbar-nav' />
                    <Navbar.Collapse id='basic-navbar-nav'>
                        <Nav className='ms-auto'>
                        {userInfo ? (
                            <>
                                <NavDropdown title={userInfo.username} id='username'>
                                    <LinkContainer to='/profile'>
                                    <NavDropdown.Item>Profile</NavDropdown.Item>
                                    </LinkContainer>
                                    <NavDropdown.Item onClick={logoutHandler}>
                                    Logout
                                    </NavDropdown.Item>
                                </NavDropdown>
                            </>
                        ) : (
                            <>
                                <LinkContainer to='/login'>
                                    <Nav.Link>
                                    <FaSignInAlt /> { AUTHSTRINGS.signIn }
                                    </Nav.Link>
                                </LinkContainer>
                                <LinkContainer to='/register'>
                                    <Nav.Link>
                                    <FaSignOutAlt /> { AUTHSTRINGS.signUp }
                                    </Nav.Link>
                                </LinkContainer>
                            </>
                        )}
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </header>
    )
}

export default Header
