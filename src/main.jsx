import React        from 'react'
import { Provider } from 'react-redux'
import ReactDOM     from 'react-dom/client'
import App          from './App.jsx'
import Login        from './screens/Login.jsx'
import Register     from './screens/Register.jsx'
import Profile      from './screens/Profile.jsx'
import PrivateRoute from './components/PrivateRoute.jsx'
import { createBrowserRouter, createRoutesFromElements, Route, RouterProvider } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css'
import './index.css'
import Home from './screens/Home.jsx'
import store from './store'

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path='/' element={<App />}>
      <Route index={true} path='/' element={<Home />} />
      <Route path='/login' element={<Login />} />
      <Route path='/register' element={<Register />} />
        <Route path='' element={<PrivateRoute />}>
          <Route path='/profile' element={<Profile />} />
        </Route>
    </Route>
))

ReactDOM.createRoot(document.getElementById('root'))
.render(
  <Provider store={ store }>
    <React.StrictMode>
      <RouterProvider router={ router } />
    </React.StrictMode>
  </Provider>
)
