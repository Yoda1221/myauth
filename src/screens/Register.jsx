import { useState, useEffect }      from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { toast }                    from 'react-toastify'
import { Button, Col, Form, Row }   from 'react-bootstrap'
import { Link, useNavigate }        from 'react-router-dom'
import { AUTHSTRINGS }              from '../services/enums'
import { setCredentials }           from '../slices/authSlice'
import Loader                       from '../components/Loader'
import { useRegisterMutation }      from '../slices/usersApiSlice'
import FormContainer                from '../components/FormContainer'

const Register = () => {
    const [username, setName]           = useState('')
    const [email, setEmail]             = useState('')
    const [password, setPassword]       = useState('')
    const [confPasswd, setConfPasswd]   = useState('')
    const dispatch                      = useDispatch()
    const navigate                      = useNavigate()
    const [register, { isLoading }]     = useRegisterMutation()
    const { userInfo } = useSelector((state) => state.auth)

    useEffect(() => {
      if (userInfo) {
        navigate('/')
      }
    }, [navigate, userInfo])

    const submitHandler = async (e) => {
        e.preventDefault()
        console.log('SUBMITTED')
        if (password !== confPasswd) toast.error('Passwords do not match!')
        else {
            try {
                const res = await register({ 
                    appName: "myAuth", username, email, password, confPasswd 
                }).unwrap()
                dispatch(setCredentials({ ...res }))
                navigate('/')
            } catch (err) {
                toast.error(err?.data?.message || err.error)
            }
        }
    }


    return (
        <FormContainer>
            <h1>{ AUTHSTRINGS.signUp }</h1>
            <Form onSubmit={submitHandler}>
                {/* USERNAME */}
                <Form.Group className='my-2' controlId='username'>
                <Form.Label>Name</Form.Label>
                <Form.Control
                    type='name'
                    placeholder='Enter name'
                    value={username}
                    onChange={(e) => setName(e.target.value)}
                ></Form.Control>
                </Form.Group>
                {/* EMAIL */}
                <Form.Group className='my-2' controlId='email'>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                        type='email'
                        placeholder='email'
                        value={ email }
                        onChange={ e => setEmail(e.target.value)}
                    ></Form.Control>
                </Form.Group>
                {/* PASSWORD */}
                <Form.Group className='my-2' controlId='password'>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type='password'
                        placeholder='Enter password'
                        value={ password }
                        onChange={ e => setPassword(e.target.value)}
                    ></Form.Control>
                </Form.Group>
                {/* CONFIRM PASSWORD */}
                <Form.Group className='my-2' controlId='confPasswd'>
                    <Form.Label>Confirm Password</Form.Label>
                    <Form.Control
                        type='password'
                        placeholder='Confirm password'
                        value={confPasswd}
                        onChange={(e) => setConfPasswd(e.target.value)}
                    ></Form.Control>
                </Form.Group>
                <Button
                    disabled={isLoading}
                    type='submit'
                    variant='info'
                    className='mt-3 rounded-pill px-3'
                >
                    { AUTHSTRINGS.signUp }
                </Button>
            </Form>
            {isLoading && <Loader />}
            <Row className='py-3'>
                <Col>
                    Already have an account? <Link to='/login'>{ AUTHSTRINGS.signIn }</Link>
                </Col>
            </Row>
        </FormContainer>
    )
}

export default Register
