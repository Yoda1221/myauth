import { useState, useEffect }      from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { toast }                    from 'react-toastify'
import { Form, Button }             from 'react-bootstrap'
import { setCredentials }           from '../slices/authSlice'
import Loader                       from '../components/Loader'
import { useUpdateUserMutation }    from '../slices/usersApiSlice'
import FormContainer                from '../components/FormContainer'
// import { Link, useNavigate } from 'react-router-dom'

const ProfileScreen = () => {
    const [username, setName]       = useState('')
    const [email, setEmail]         = useState('')
    const dispatch                  = useDispatch()
    const { userInfo } = useSelector((state) => state.auth)
    const [updateProfile, { isLoading }] = useUpdateUserMutation()
    //const [password, setPassword]     = useState('')
    //const [confirmPassword, setConfirmPassword] = useState('')

    useEffect(() => {
        setName(userInfo.username)
        setEmail(userInfo.email)
    }, [userInfo.email, userInfo.username])

    const submitHandler = async (e) => {
        e.preventDefault()
        /* if (password !== confirmPassword) {
        toast.error('Passwords do not match')
        } else { */
        try {
            const res = await updateProfile({
                appName: "myAuth", 
                id: userInfo.id,
                username,
                email,
                //password,
            }).unwrap()
            dispatch(setCredentials({ ...res }))
            toast.success('Profile updated successfully')
        } catch (err) {
            toast.error(`ERROR , ${err?.data?.message || err.error}`)
        }
        //}
    }

    return (
        <FormContainer>
            <h1>Update Profile</h1>
            <Form onSubmit={submitHandler}>
                <Form.Group className='my-2' controlId='username'>
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                        type='name'
                        placeholder='Enter name'
                        value={username}
                        onChange={(e) => setName(e.target.value)}
                    ></Form.Control>
                </Form.Group>
                <Form.Group className='my-2' controlId='email'>
                    <Form.Label>Email Address</Form.Label>
                    <Form.Control
                        type='email'
                        placeholder='Enter email'
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    ></Form.Control>
                </Form.Group>
        {/*         <Form.Group className='my-2' controlId='password'>
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type='password'
                    placeholder='Enter password'
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                ></Form.Control>
                </Form.Group>

                <Form.Group className='my-2' controlId='confirmPassword'>
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control
                    type='password'
                    placeholder='Confirm password'
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                ></Form.Control>
                </Form.Group> */}

                <Button type='submit' variant='warning' className='mt-3 rounded-pill px-3'>
                Update
                </Button>
                {isLoading && <Loader />}
            </Form>
        </FormContainer>
    )
}

export default ProfileScreen
